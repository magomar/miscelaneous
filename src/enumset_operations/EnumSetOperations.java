/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package enumset_operations;

import java.util.EnumSet;
import java.util.Set;
import jumbo_vs_regular_enum.MyRegularEnum;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class EnumSetOperations {
    public static void main(String[] args) {
        Set<MyRegularEnum> s1 = EnumSet.of(MyRegularEnum.reg0, MyRegularEnum.reg1);
        Set<MyRegularEnum> s2 = EnumSet.of(MyRegularEnum.reg1, MyRegularEnum.reg2);
        Set<MyRegularEnum> s3 = EnumSet.copyOf(s1);
        s3.retainAll(s2);
        System.out.println("S1 = " + s1);
        System.out.println("S2 = " + s2);
        System.out.println("S1 AND S2 = " + s3);
        
    }
}
