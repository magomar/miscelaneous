package observable_map;


import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Map<String, String> map = new HashMap<String, String>();
        ObservableMap<String, String> observableMap = FXCollections
                .observableMap(map);
        observableMap.addListener(new MapChangeListener() {
            @Override
            public void onChanged(MapChangeListener.Change change) {
                System.out.println("change! " + change);
            }
        });
        observableMap.put("key 1", "value 1");
        map.put("key 2", "value 2");
        map.put("key 3", "value 3");

    }
}

