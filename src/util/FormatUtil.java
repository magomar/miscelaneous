package util;

import java.math.BigDecimal;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class FormatUtil {

    public static double formatDouble(double d) {
        BigDecimal bd = new BigDecimal(Double.toString(d));
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }
}
