package util;

/**
 * The
 * <code>CharUtil</code> class includes a number of useful utilities in dealing
 * with characters. For example, testing whether a character is within a certain
 * set, converting between characters and digits in various base systems, etc.
 *
 * @author Ben L. Titzer
 */
public class CharUtil {

    public static final char[] HEX_CHARS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    public static final char[] LOW_HEX_CHARS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /**
     * The
     * <code>isHexDigit()</code> method checks whether the specified character
     * represents a valid hexadecimal character. This method is
     * case-insensitive.
     *
     * @param c the character to check
     * @return true if the specified character is a valid hexadecimal value;
     * false otherwise
     */
    public static boolean isHexDigit(char c) {
        switch (c) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case 'a':
            case 'b':
            case 'c':
            case 'd':
            case 'e':
            case 'f':
            case 'A':
            case 'B':
            case 'C':
            case 'D':
            case 'E':
            case 'F':
                return true;
        }
        return false;
    }

    public static char toUpperHexChar(int digitValue) {
        if (digitValue < 10) {
            return (char) (digitValue | '0');
        }
        return (char) ('A' + digitValue - 10);
    }

    public static char toLowerHexChar(int digitValue) {
        if (digitValue < 10) {
            return (char) (digitValue | '0');
        }
        return (char) ('a' + digitValue - 10);
    }

    /**
     * The
     * <code>hexValueOf()</code> method converts a character into an integer
     * value according to the hexadecimal base system.
     *
     * @param c the character to convert
     * @return the value of the character in the hexadecimal base system
     */
    public static int hexValueOf(char c) {
        return Character.digit(c, 16);
    }

    /**
     * The
     * <code>isDecDigit()</code> method checks whether the specified character
     * represents a valid decimal character. .
     *
     * @param c the character to check
     * @return true if the specified character is a valid decimal digit; false
     * otherwise
     */
    public static boolean isDecDigit(char c) {
        if (c < '0') {
            return false;
        }
        return c <= '9';
    }

    /**
     * The
     * <code>decValueOf()</code> method converts a character into an integer
     * value according to the decimal base system.
     *
     * @param c the character to convert
     * @return the value of the character in the decimal base system
     */
    public static int decValueOf(char c) {
        return Character.digit(c, 10);
    }

    /**
     * The
     * <code>isOctDigit()</code> method checks whether the specified character
     * represents a valid octal character.
     *
     * @param c the character to check
     * @return true if the specified character is a valid octal digit; false
     * otherwise
     */
    public static boolean isOctDigit(char c) {
        if (c < '0') {
            return false;
        }
        return c <= '7';
    }

    /**
     * The
     * <code>octValueOf()</code> method converts a character into an integer
     * value according to the octal base system.
     *
     * @param c the character to convert
     * @return the value of the character in the octal base system
     */
    public static int octValueOf(char c) {
        return Character.digit(c, 8);
    }

    /**
     * The
     * <code>isBinDigit()</code> method checks whether the specified character
     * represents a valid binary character.
     *
     * @param c the character to check
     * @return true if the specified character is a valid binary digit; false
     * otherwise
     */
    public static boolean isBinDigit(char c) {
        return c == '0' || c == '1';
    }

    /**
     * The
     * <code>binValueOf()</code> method converts a character into an integer
     * value according to the binary base system.
     *
     * @param c the character to convert
     * @return the value of the character in the binary base system
     */
    public static int binValueOf(char c) {
        return c == '0' ? 0 : 1;
    }
}
