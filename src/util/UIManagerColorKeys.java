package util;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * Helper (utility) class to generate a list of UIManager color keys.
 *
 * Author: Mario Gómez Martínez <magomar@gmail.com>
 */
public class UIManagerColorKeys {
    public static void main(String[] args) throws Exception {
        List<String> colorKeys = new ArrayList<>();
        Set<Entry<Object, Object>> entries = UIManager.getLookAndFeelDefaults().entrySet();
        for (Entry entry : entries) {
            if (entry.getValue() instanceof Color) {
                colorKeys.add((String) entry.getKey());
            }
        }

        // sort the color keys
        Collections.sort(colorKeys);

        // print the color keys
        for (String colorKey : colorKeys) {
            System.out.println(colorKey);
        }

    }
}