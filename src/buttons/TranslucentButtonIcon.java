package buttons;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;

/**
 * @author Mario Gómez Martínez <magomar@gmail.com>
 */
public class TranslucentButtonIcon implements Icon {
    private static final Color TL = new Color(1f, 1f, 1f, .2f);
    private static final Color BR = new Color(0f, 0f, 0f, .4f);
    private static final Color ST = new Color(1f, 1f, 1f, .2f);
    private static final Color SB = new Color(1f, 1f, 1f, .1f);
    private Color ssc;
    private Color bgc;
    private int r = 8;

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        int w = c.getWidth();
        int h = c.getHeight();

        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Shape area = new RoundRectangle2D.Float(x, y, w - 1, h - 1, r, r);
        ssc = TL;
        bgc = BR;
        if (c instanceof AbstractButton) {
            ButtonModel m = ((AbstractButton) c).getModel();
            if (m.isPressed()) {
                ssc = SB;
                bgc = ST;
            } else if (m.isRollover()) {
                ssc = ST;
                bgc = SB;
            }
        }
        g2.setPaint(new GradientPaint(x, y, ssc, x, y + h, bgc, true));
        g2.fill(area);
        g2.setPaint(BR);
        g2.draw(area);
        g2.dispose();
    }

    @Override
    public int getIconWidth() {
        return 100;
    }

    @Override
    public int getIconHeight() {
        return 24;
    }
}