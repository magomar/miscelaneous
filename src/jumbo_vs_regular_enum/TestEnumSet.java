/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jumbo_vs_regular_enum;

import java.util.EnumSet;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class TestEnumSet {
    final static int n = 10000000;

    public static void main(String[] args) {
        
        final long startTime = System.nanoTime();
        final long endTime;
        try {
            testRegular();
        } finally {
            endTime = System.nanoTime();
        }
        final long duration = endTime - startTime;
        System.out.println("Duration = " + duration);
    }

    static void testJumbo() {
        EnumSet<MyJumboEnum> es = EnumSet.range(MyJumboEnum.jum30, MyJumboEnum.jum50);
        for (int i = 0; i < n; i++) { 
            int mask = 0;
            for (MyJumboEnum e : EnumSet.range(MyJumboEnum.jum40, MyJumboEnum.jum80)) {
                es.contains(e);
                mask |=1<<e.ordinal();
            }
        }
    }

    static void testRegular() {
        EnumSet<MyRegularEnum> es = EnumSet.range(MyRegularEnum.reg30, MyRegularEnum.reg40);
        EnumSet<MyRegularEnum> es2 = EnumSet.range(MyRegularEnum.reg41, MyRegularEnum.reg50);
        for (int i = 0; i < n; i++) {
            int mask = 0;
            for (MyRegularEnum e : EnumSet.range(MyRegularEnum.reg0, MyRegularEnum.reg19)) {
                es.contains(e);
                mask |=1<<e.ordinal();
            }
            int mask2 = 0;
            for (MyRegularEnum e : EnumSet.range(MyRegularEnum.reg20, MyRegularEnum.reg40)) {
                es2.contains(e);
                mask2 |=1<<e.ordinal();
            }
        }
    }
}
