/** 
 * Copyright (c) 2010 Keang Ltd
 * 
 * This file is part of the Hexagonal Buttons package.
 *
 * The Hexagonal Buttons package is free software: you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The Hexagonal Buttons package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with the Hexagonal Buttons package.  If not, see <http://www.gnu.org/licenses/>.
 */
package hex_layout;

import java.awt.*;

/**
 * A collection of utility methods for Hexagonal buttons
 * 
 * @author A.G.Docherty
 * @date 19 Jun 2009
 * @version 1.1
 */
public class HexBtnUtilities
{
public enum HexOrientation
{
VERTICAL_SIDES 
    {
    @Override
    int sizeOverPoints(int w, int h)
        {
        return h;
        }

    @Override
    int sizeOverSides(int w, int h)
        {
        return w;
        }
    
    @Override
    Polygon createHexagon(int[] hexA, int[] hexB)
        {
        return new Polygon(hexA, hexB, hexA.length);
        
        }

    @Override
    Rectangle viewRect(int[] x, int[] y)
        {
        return new Rectangle(x[5], y[5], x[1]-x[5], y[4]-y[5]);
        }
    
    @Override
    Polygon[][] getHighLights(Polygon h, Polygon b)
        {
        Polygon[][] hi = new Polygon[2][3];

        int[] hx = h.xpoints;
        int[] hy = h.ypoints;
        int[] bx = b.xpoints;
        int[] by = b.ypoints;
        
        hi[0][0] = new Polygon (new int[] {hx[1], hx[2], bx[2], bx[1]}, new int[] {hy[1], hy[2], by[2], by[1]}, 4);
        hi[0][1] = new Polygon (new int[] {hx[2], hx[3], bx[3], bx[2]}, new int[] {hy[2], hy[3], by[3], by[2]}, 4);
        hi[0][2] = new Polygon (new int[] {hx[3], hx[4], bx[4], bx[3]}, new int[] {hy[3], hy[4], by[4], by[3]}, 4);

        hi[1][0] = new Polygon (new int[] {hx[4], hx[5], bx[5], bx[4]}, new int[] {hy[4], hy[5], by[5], by[4]}, 4);
        hi[1][1] = new Polygon (new int[] {hx[5], hx[0], bx[0], bx[5]}, new int[] {hy[5], hy[0], by[0], by[5]}, 4);
        hi[1][2] = new Polygon (new int[] {hx[0], hx[1], bx[1], bx[0]}, new int[] {hy[0], hy[1], by[1], by[0]}, 4);
        
        return hi;
        }

    },
HORIZONTAL_SIDES
    {
    Polygon createHexagon(int[] hexA, int[] hexB)
        {
        return new Polygon(hexB, hexA, hexA.length);
        
        }
    
    @Override
    int sizeOverPoints(int w, int h)
        {
        return w;
        }

    @Override
    int sizeOverSides(int w, int h)
        {
        return h;
        }

    @Override
    Rectangle viewRect(int[] x, int[] y)
        {
        return new Rectangle(x[1], y[5], x[4]-x[1], y[1]-y[5]);
        }
    
    @Override
    Polygon[][] getHighLights(Polygon h, Polygon b)
        {
        Polygon[][] hi = new Polygon[2][3];

        int[] hx = h.xpoints;
        int[] hy = h.ypoints;
        int[] bx = b.xpoints;
        int[] by = b.ypoints;
        
        hi[0][0] = new Polygon (new int[] {hx[1], hx[2], bx[2], bx[1]}, new int[] {hy[1], hy[2], by[2], by[1]}, 4);
        hi[0][1] = new Polygon (new int[] {hx[2], hx[3], bx[3], bx[2]}, new int[] {hy[2], hy[3], by[3], by[2]}, 4);
        hi[0][2] = new Polygon (new int[] {hx[3], hx[4], bx[4], bx[3]}, new int[] {hy[3], hy[4], by[4], by[3]}, 4);

        hi[1][0] = new Polygon (new int[] {hx[4], hx[5], bx[5], bx[4]}, new int[] {hy[4], hy[5], by[5], by[4]}, 4);
        hi[1][1] = new Polygon (new int[] {hx[5], hx[0], bx[0], bx[5]}, new int[] {hy[5], hy[0], by[0], by[5]}, 4);
        hi[1][2] = new Polygon (new int[] {hx[0], hx[1], bx[1], bx[0]}, new int[] {hy[0], hy[1], by[1], by[0]}, 4);
        
        return hi;
        }

    };

abstract int sizeOverPoints(int w, int h);    
abstract int sizeOverSides(int w, int h);    
abstract Polygon createHexagon(int[] hexA, int[] hexB) ;
abstract Rectangle viewRect(int[] x, int[] y) ;
abstract Polygon[][] getHighLights(Polygon h, Polygon b);
} 

/**
 * Creates a hexagon based on the given orientation, origin and size
 * 
 * @param orientation - the hexagons orientation
 * @param originX - the x axis origin. This is the top left corner of an imaginary square enclosing the hexagon
 * @param originY - the y axis origin. This is the top left corner of an imaginary square enclosing the hexagon
 * @param w - the maximum width of the hexagon
 * @param h - the maximum height of the hexagon
 * @return the co-ordinates of the hexagon
 */
static Polygon getHexagon(HexOrientation orientation, int originX, int originY, int w, int h)
    {
    w--;
    h--;

    int p = orientation.sizeOverPoints(w, h);
    int s = orientation.sizeOverSides(w, h);
    
    int q =(int)((p * 0.25f)+0.5f);
    
    int  b1 = originY;
    int  b2 = originY + q;
    int  b3 = originY + p - q;
    int  b4 = originY + p;
    int  a1 = originX;
    int  a2 = originX + (s+1)/2;
    int  a3 = originX + s;
    
    int nPoints = 6;
    int[] hexA = new int[nPoints];
    int[] hexB = new int[nPoints];
    
    hexA[0] = a2;
    hexB[0] = b1;
    hexA[1] = a3;
    hexB[1] = b2;
    hexA[2] = a3;
    hexB[2] = b3;
    hexA[3] = a2;
    hexB[3] = b4;
    hexA[4] = a1;
    hexB[4] = b3;
    hexA[5] = a1;
    hexB[5] = b2;

    return orientation.createHexagon(hexA, hexB);
    }

/**
 * Draws a dashed polgon in the given graphics context
 * 
 * @param g the graphics context
 * @param p the polygon to draw
 */
static void drawDashedPolygon(Graphics g, Polygon p) 
    {
    int[] x = p.xpoints;
    int[] y = p.ypoints;
    
    for ( int i = 0; i < x.length; i++ )
        {
        int dx, dy;
        int i2;
        
        if ( i == x.length - 1 )
            {
            i2 = 0;
            }
        else
            {
            i2 = i+1;
            }
        
        dx = x[i2] - x[i];
        dy = y[i2] - y[i];
        
        if ( dx == 0 )
            {
            int origin = dy > 0 ? y[i]:y[i2];
            dy = Math.abs(dy);
            
//            System.out.println("HexBtnUtilities.drawDashedPolygon(): x = "+x[i]+", origin = "+origin);
            for ( int j = 0; j < dy; j+=2 )
                {
//                System.out.println("x = "+x[i]+", y = "+(origin+j));
                g.fillRect(x[i], origin+j, 1, 1);
                }
            }
        else if ( dy == 0 )
            {
            int origin = dx > 0 ? x[i]:x[i2];
            dx = Math.abs(dx);
            
            for ( int j = 0; j < dx; j+=2 )
                {
                g.fillRect(origin+j, y[i], 1, 1);
                }
            }
        else 
            {
            int originx = x[i];
            int originy = y[i];
            int hyp = (int)Math.sqrt(Math.abs((dx*dx))+Math.abs((dy*dy)));

            float offsetX = 0f;
            float offsetY = 0f;
            
            float incX = dx*2.0f/hyp;
            float incY = dy*2.0f/hyp;
            
            for ( int j = 0; j < hyp; j+=2 )
                {
                offsetX += incX;
                offsetY += incY;
                
                g.fillRect(originx+(int)offsetX, originy+(int)offsetY, 1, 1);
                }
            }
        }
    }


}
