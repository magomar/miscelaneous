/**
 * Copyright (c) 2010 Keang Ltd
 *
 * This file is part of the Hexagonal Buttons package.
 *
 * The Hexagonal Buttons package is free software: you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Hexagonal Buttons package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the Hexagonal Buttons package.  If not, see <http://www.gnu.org/licenses/>.
 */
package hex_layout;

import hex_layout.HexBtnUtilities.HexOrientation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;

/**
 * A six sided toggle button. This is not guaranteed to be a perfect hexagon, it is just guaranteed to have six sides in
 * the form of a hexagon. To be a perfect hexagon the size of this component must have a height to width ratio of
 * 1 to 0.866
 * <p/>
 * The hexagon can be in one of two orientations:
 * <p/>
 * <pre>
 *     Vertical          Horizontal
 *         *                *  *
 *      *     *           *      *
 *      *     *             *  *
 *         *
 * </pre>
 *
 * @author A.G.Docherty
 * @version 1.2
 * @date 19 Jun 2009
 */
public class HexToggleButton extends JToggleButton implements IHexagonal {
    private static final long serialVersionUID = 4865976127980106774L;
    private static final int FOCUS_INSET = 6;
    private static final int HILITE_INSET = 2;
    private Polygon hexBoundary = new Polygon();
    private Polygon hexFocus = new Polygon();
    private Polygon[][] hexHilite = new Polygon[2][3];
    private HexOrientation orientation = HexOrientation.HORIZONTAL_SIDES;
    private boolean requiresHexCalc = true;
    private transient Color cachedSelectedColor = null;


    /**
     * Creates a button with no icon or text
     */
    public HexToggleButton() {
        // default constructor
    }

    /**
     * Creates a button with the given icon
     *
     * @param icon the icon to display on the button
     */
    public HexToggleButton(Icon icon) {
        super(icon);
    }

    /**
     * Creates a button with the given text
     *
     * @param text the text to display on the button
     */
    public HexToggleButton(String text) {
        super(text);
    }

    /**
     * Creates a button with the given Action
     *
     * @param action the action for this button
     */
    public HexToggleButton(Action action) {
        super(action);
    }

    /**
     * Creates a button with the given icon in the given toggled state
     *
     * @param icon    the icon to display on the button
     * @param toggled true if the button is toggled
     */
    public HexToggleButton(Icon icon, boolean toggled) {
        super(icon, toggled);
    }

    /**
     * Creates a button with the given text in the given toggled state
     *
     * @param text    the text to display on the button
     * @param toggled true if the button is toggled
     */
    public HexToggleButton(String text, boolean toggled) {
        super(text, toggled);
    }

    /**
     * Creates a button with the given text and icon
     *
     * @param text the text to display on the button
     * @param icon the icon to display on the button
     */
    public HexToggleButton(String text, Icon icon) {
        super(text, icon);
    }

    /**
     * Creates a button with the given text and icon and in the given toggled state
     *
     * @param text    the text to display on the button
     * @param icon    the icon to display on the button
     * @param toggled true if the button is toggled
     */
    public HexToggleButton(String text, Icon icon, boolean toggled) {
        super(text, icon, toggled);
    }

    /**
     * Gets the orientation of the hexagon shape.
     *
     * @return the orientation
     */
    public HexOrientation getOrientation() {
        return orientation;
    }

    /**
     * Sets the orientation of the hexagon shape.
     * Two orientations are available:
     * <p/>
     * 1. VERTICAL_SIDES - sides are vertical and points are top and bottom
     * 2. HORIZONTAL_SIDES - top and bottom are horizontal and points are left and right
     * <p/>
     * By default orientation is VERTICAL_SIDES
     *
     * @param o - the orientation
     */
    @Override
    public void setOrientation(HexOrientation o) {
        if (orientation == o)
            return;

        orientation = o;
        requiresHexCalc = true;
    }

    @Override
    public boolean contains(Point p) {
        return hexBoundary.contains(p);
    }

    @Override
    public boolean contains(int x, int y) {
        return hexBoundary.contains(x, y);
    }

    @Override
    public void setSize(Dimension d) {
        super.setSize(d);
        requiresHexCalc = true;
    }

    @Override
    public void setSize(int w, int h) {
        super.setSize(w, h);
        requiresHexCalc = true;
    }

    @Override
    public void setBounds(int x, int y, int width, int height) {
        super.setBounds(x, y, width, height);
        requiresHexCalc = true;
    }

    @Override
    public void setBounds(Rectangle r) {
        super.setBounds(r);
        requiresHexCalc = true;
    }

    @Override
    public void setBackground(Color c) {
        cachedSelectedColor = null;
        super.setBackground(c);
    }

    @Override
    protected void processMouseEvent(MouseEvent e) {
        if (contains(e.getPoint()))
            super.processMouseEvent(e);
    }

    /**
     * Calculate the coordinates of the various parts of the button
     */
    private void calculateCoords() {
        int w = getWidth();
        int h = getHeight();
        hexBoundary = HexBtnUtilities.getHexagon(orientation, 0, 0, w, h);
        hexHilite = orientation.getHighLights(HexBtnUtilities.getHexagon(orientation, HILITE_INSET, HILITE_INSET, w - HILITE_INSET * 2, h - HILITE_INSET * 2), hexBoundary);
        hexFocus = HexBtnUtilities.getHexagon(orientation, FOCUS_INSET, FOCUS_INSET, w - FOCUS_INSET * 2, h - FOCUS_INSET * 2);
    }

    @Override
    protected void paintComponent(Graphics g) {
        Color oldColor = g.getColor();  // Make no net change to g

        if (requiresHexCalc) {
            calculateCoords();
            requiresHexCalc = false;
        }

        Graphics2D g2d = (Graphics2D) g;
        Color c = getBackground();

        g2d.setColor(c);

        boolean raised = !isSelected();

        if (!raised) {
            Color c1 = getBackground();
            Color c2 = UIManager.getColor("ToggleButton.highlight");


            if (cachedSelectedColor == null) {
                int r1 = c1.getRed(), r2 = c2.getRed();
                int g1 = c1.getGreen(), g2 = c2.getGreen();
                int b1 = c1.getBlue(), b2 = c2.getBlue();

                cachedSelectedColor = new Color(
                        Math.min(r1, r2) + Math.abs(r1 - r2) / 2,
                        Math.min(g1, g2) + Math.abs(g1 - g2) / 2,
                        Math.min(b1, b2) + Math.abs(b1 - b2) / 2
                );
            }

            g.setColor(cachedSelectedColor);

//        g2d.setColor(c.brighter());
        }

        // fill the area
        g2d.fillPolygon(hexBoundary);

        // add highlights
        g2d.setColor(raised ? Color.darkGray : Color.white);

        for (int i = 0; i < hexHilite[0].length; i++) {
            g2d.fillPolygon(hexHilite[0][i]);
        }

        g2d.setColor(raised ? Color.white : Color.darkGray);

        for (int i = 0; i < hexHilite[1].length; i++) {
            g2d.fillPolygon(hexHilite[1][i]);
        }

        // draw the outline
        g2d.setColor(Color.black);
        g2d.drawPolygon(hexBoundary);

        // add focus marker
        if (hasFocus() && isFocusPainted()) {
//        g2d.setColor(getForeground().brighter().brighter());
            g2d.setColor(Color.gray);
            HexBtnUtilities.drawDashedPolygon(g2d, hexFocus);
        }

        // add the text
        FontMetrics fm = getFontMetrics(getFont());
        int[] hx = hexFocus.xpoints;
        int[] hy = hexFocus.ypoints;

        Rectangle viewR = orientation.viewRect(hx, hy);
        Rectangle iconR = new Rectangle();
        Rectangle textR = new Rectangle();

        Icon icon = getIcon();

        if (!raised) {
            Icon img = getPressedIcon();

            if (img != null)
                icon = img;
        }

        String text = getText();

        SwingUtilities.layoutCompoundLabel(this, fm, text, icon,
                SwingUtilities.CENTER, SwingUtilities.CENTER, SwingUtilities.BOTTOM, SwingUtilities.CENTER,
                viewR, iconR, textR, 0);

        g2d.setColor(getForeground());

    /*
    if ( text != null )
        g2d.drawString(text, textR.x, textR.y+fm.getAscent());

    if ( icon != null )
        icon.paintIcon(this, g, iconR.x, iconR.y);
    */
        if (icon != null) {
            // only paint the icon if the view is big enough
            if (iconR.width <= viewR.width && iconR.height <= viewR.height)
                icon.paintIcon(this, g, iconR.x, iconR.y);
            else {
                // force the text position to be recalculated as the icon is no longer being drawn
                icon = null;
                textR.width = viewR.width + 1;
            }
        }

//    System.out.println("HexButton: bounds = "+viewR+", text = "+textR+", icon = "+iconR);
        if (text != null) {
            Font oldFont = g2d.getFont();
            Font f = getFont();
            int fSize = f.getSize();

            while (textR.width > viewR.width || textR.height > viewR.height) {
                fSize--;
                f = f.deriveFont((float) fSize);
                fm = getFontMetrics(f);

                Rectangle2D r2d = fm.getStringBounds(text, g2d);

                textR.width = (int) (r2d.getWidth() + 0.5);
                textR.height = (int) (r2d.getHeight() + 0.5);
                textR.x = viewR.x + ((viewR.width - textR.width) / 2);
                textR.y = viewR.y + ((viewR.height - textR.height) / 2);
            }

            g2d.setFont(f);
            g2d.drawString(text, textR.x, textR.y + fm.getAscent());
            g2d.setFont(oldFont);
        }

        g.setColor(oldColor);
    }

    @Override
    protected void paintBorder(Graphics g) {
        // do not paint a border
    }

}
