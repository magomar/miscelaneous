/** 
 * Copyright (c) 2010 Keang Ltd
 * 
 * This file is part of the Hexagonal Buttons package.
 *
 * The Hexagonal Buttons package is free software: you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * The Hexagonal Buttons package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with the Hexagonal Buttons package.  If not, see <http://www.gnu.org/licenses/>.
 */
package hex_layout;

import hex_layout.HexBtnUtilities.HexOrientation;

/**
 * A class implements this interface if it represents a hexagonal object that can be in
 * different orientations
 * 
 * @author A.G.Docherty
 * @date 13 Jun 2009
 *
 */
public interface IHexagonal
{
/**
 * Sets the orientation of the hexagon shape.
 * Two orientations are available:
 * 
 * 1. VERTICAL_SIDES - sides are vertical and points are top and bottom   
 * 2. HORIZONTAL_SIDES - top and bottom are horizontal and points are left and right
 * 
 * By default orientation is VERTICAL_SIDES
 * 
 * @param o - the orientation
 */
public void setOrientation(HexOrientation o) ;

}
