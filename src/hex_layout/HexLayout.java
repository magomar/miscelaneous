/**
 * Copyright (c) 2010 Keang Ltd
 *
 * This file is part of the Hexagonal Buttons package.
 *
 * The Hexagonal Buttons package is free software: you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as published 
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Hexagonal Buttons package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the Hexagonal Buttons package.  If not, see <http://www.gnu.org/licenses/>.
 */
package hex_layout;


import hex_layout.HexBtnUtilities.HexOrientation;

import java.awt.*;

/**
 * The Hexagonal layout manager lays out components in a hexagonal grid. If components implement IHexagonal they
 * will be rotated by this layout manager to suit the selected layout direction. Components not implementing
 * IHexagonal and/or Components of any other shape can be laid out but their visible areas may overlap.
 * <p/>
 * This layout manager supports two layout directions: Vertical and horizontal, and three layout
 * styles: Wavey, Straight Min First and Straight Max First.
 * <p/>
 * <pre>
 * Direction                                                                        /\  /\
 * Vertical = hexagonal components are rotated so that their sides are vertical ie |  ||  |
 *                                                                                  \/  \/
 *                                                                                      __
 * Horizontal = hexagonal components are rotated so that their sides are horizontal ie /  \
 *                                                                                     \  /
 *                                                                                      ==
 *                                                                                     /  \
 *                                                                                     \  /
 *                                                                                      --
 * </pre>
 * <p/>
 * Style: (all examples shown are for vertical layout)<br>
 * Wavey = the same number of components per line
 * <pre>
 * ie  * * * *
 *      * * * *
 *     * * * *
 *      * * * *
 * </pre>
 * <p/>
 * Straight Min First = Alternate lines, starting with the first line, have one less component.
 * <pre>
 * ie   * * *
 *     * * * *
 *      * * *
 *     * * * *
 * </pre>
 * <p/>
 * Straight Max First = Alternate lines, starting with the second line, have one less component.
 * <pre>
 * ie  * * * *
 *      * * *
 *     * * * *
 *      * * *
 * </pre>
 * <p/>
 * The size of the layout area is described in terms of the number of components per line and the number of lines.
 * Either dimension can be zero, in which that dimension is sized to fit the number of components in the panel.
 * If both dimensions are specified and layout area is sized to the given dimension. The components are placed along
 * each line until the max line length is reached. If more components are added than the size of the area permits
 * the number of lines is increased.
 * <p/>
 * For Vertical direction the components are added from left to right. For Horizontal direction the
 * components are added from top to bottom.
 * <p/>
 * By default the hexagons will be stretched to fill the window in both the X and Y axis,
 * if you require pure hexagons call <code>setStretchy(false)</code>.
 *
 * @author A.G.Docherty
 * @version 1.1
 * @date 19 Jun 2009
 */
public class HexLayout implements LayoutManager, java.io.Serializable {
    private static final long serialVersionUID = -858342723067286796L;
    private static final float ASPECT_RATIO = 0.866f;
    /**
     * This is the gap (in pixels) which specifies the space
     * between components.  They can be changed at any time.
     * This should be a non-negative integer.
     */
    int cgap;
    /**
     * This is the number of rows specified for the grid.  The number
     * of rows can be changed at any time.
     * This should be a non negative integer, where '0' means
     * 'any number' meaning that the number of Rows in that
     * dimension depends on the other dimension.
     */
    int cpl;
    /**
     * This is the number of columns specified for the grid.  The number
     * of columns can be changed at any time.
     * This should be a non negative integer, where '0' means
     * 'any number' meaning that the number of Columns in that
     * dimension depends on the other dimension.
     */
    int lines;
    /**
     * Determines whether or not to allow stretched hexagons.
     * Settings this to true will force the components to be proper hexagons
     */
    private boolean stretchy = true;
    /**
     * This is the orientation of the hexagonal components. If components aren't
     * currently in the correct orientation they will be changed to suit this layout
     */
    private HexOrientation orientation;
    private HexLayoutStyle layoutStyle;
    private AbstractLayout layout;

    /**
     * Creates a hex layout with a default of one column per component,
     * in a single row.
     */
    public HexLayout() {
        this(1, 0, 0, HexOrientation.VERTICAL_SIDES, HexLayoutStyle.WAVEY);
    }

    /**
     * Creates a hex layout with the specified number of rows and
     * columns. All components in the layout are given equal size.
     * <p/>
     * One, but not both, of <code>rows</code> and <code>cols</code> can
     * be zero, which means that any number of objects can be placed in a
     * row or in a column.
     *
     * @param r the rows, with the value zero meaning
     *          any number of rows.
     * @param c the columns, with the value zero meaning
     *          any number of columns.
     */
    public HexLayout(int r, int c) {
        this(r, c, 0, HexOrientation.VERTICAL_SIDES, HexLayoutStyle.WAVEY);
    }

    /**
     * Creates an offset hex layout with the specified number of lines and
     * components per line. All components in the layout are given equal size.
     * <p/>
     * In addition, the gap between components is set to the
     * specified value.
     * <p/>
     * If <code>nComps</code> is zero any number of objects can be placed on a line.
     * If <code>lines</code> is zero there can be any number of lines.
     * If both are zero the layout will be approximately a square layout.
     * <p/>
     * All <code>HexLayout</code> constructors defer to this one.
     *
     * @param nComps the rows, with the value zero meaning
     *               any number of rows
     * @param nLines the columns, with the value zero meaning
     *               any number of columns
     * @param hgap   the gap around the component
     * @param o      the orientation of the hexagon ie vertical or horizontal
     */
    public HexLayout(int nComps, int nLines, int hgap, HexOrientation o, HexLayoutStyle style) {
        this.cpl = nComps;
        this.lines = nLines;
        this.cgap = hgap;
        this.orientation = o;
        this.layoutStyle = style;

        if (orientation == HexOrientation.VERTICAL_SIDES)
            layout = new VerticalLayout();
        else
            layout = new HorizontalLayout();
    }

    /**
     * Gets the number of components per line in this layout.
     *
     * @return the number of components per line in this layout
     */
    public int getComponentsPerLine() {
        return cpl;
    }

    /**
     * Sets the number of components per line in this layout to the specified value.
     *
     * @param c the number of components per line in this layout
     */
    public void setComponentsPerLine(int c) {
        this.cpl = c;
    }

    /**
     * Gets the number of lines in this layout.
     *
     * @return the number of lines in this layout
     */
    public int getLines() {
        return lines;
    }

    /**
     * Sets the number of lines in this layout to the specified value.
     *
     * @param n the number of lines in this layout
     */
    public void setLines(int n) {
        this.lines = n;
    }

    /**
     * Gets the gap between components.
     *
     * @return the gap between components
     */
    public int getGap() {
        return cgap;
    }

    /**
     * Sets the gap between components to the specified value.
     *
     * @param gap the gap between components
     */
    public void setGap(int gap) {
        this.cgap = gap;
    }

    /**
     * Returns true if hexagons can be stretched to fill the whole panel
     *
     * @return true if components can be stretched
     */
    public boolean isStretchy() {
        return this.stretchy;
    }

    /**
     * Sets whether hexagons have to be pure or whether they can be stretched to fill the whole panel
     *
     * @param stretch true to allow components to be stretched, false for pure hexagons
     */
    public void setStretchy(boolean stretch) {
        this.stretchy = stretch;
    }

    /**
     * Adds the specified component with the specified name to the layout.
     *
     * @param name the name of the component
     * @param comp the component to be added
     */
    public void addLayoutComponent(String name, Component comp) {
        // do nothing
    }

    /**
     * Removes the specified component from the layout.
     *
     * @param comp the component to be removed
     */
    public void removeLayoutComponent(Component comp) {
        // do nothing
    }

    /**
     * Determines the preferred size of the container argument using
     * this grid layout.
     * <p/>
     * The preferred width of a grid layout is the largest preferred width
     * of all of the components in the container times the number of columns,
     * plus the horizontal padding times the number of columns minus one,
     * plus the left and right insets of the target container, plus the width
     * of half a component if there is more than one row.
     * <p/>
     * The preferred height of a grid layout is the largest preferred height
     * of all of the components in the container plus three quarters of the
     * largest minimum height of all of the components in the container times
     * the number of rows greater than one,
     * plus the vertical padding times the number of rows minus one, plus
     * the top and bottom insets of the target container.
     *
     * @param parent the container in which to do the layout
     * @return the preferred dimensions to lay out the
     *         subcomponents of the specified container
     * @see java.awt.GridLayout#minimumLayoutSize
     * @see java.awt.Container#getPreferredSize()
     */
    public Dimension preferredLayoutSize(Container parent) {
        synchronized (parent.getTreeLock()) {
            return layout.preferredLayoutSize(parent);
        }
    }

    /**
     * Determines the minimum size of the container argument using this
     * grid layout.
     * <p/>
     * The minimum width of a grid layout is the largest minimum width
     * of all of the components in the container times the number of columns,
     * plus the horizontal padding times the number of columns minus one,
     * plus the left and right insets of the target container, plus the width
     * of half a component if there is more than one row.
     * <p/>
     * The minimum height of a grid layout is the largest minimum height
     * of all of the components in the container plus three quarters of the
     * largest minimum height of all of the components in the container times
     * the number of rows greater than one,
     * plus the vertical padding times the number of rows minus one, plus
     * the top and bottom insets of the target container.
     *
     * @param parent the container in which to do the layout
     * @return the minimum dimensions needed to lay out the
     *         subcomponents of the specified container
     * @see java.awt.GridLayout#preferredLayoutSize
     * @see java.awt.Container#doLayout
     */
    public Dimension minimumLayoutSize(Container parent) {
        synchronized (parent.getTreeLock()) {
            return layout.minimumLayoutSize(parent);
        }
    }

    /**
     * Lays out the specified container using this layout.
     * <p/>
     * This method reshapes the components in the specified target
     * container in order to satisfy the constraints of the
     * <code>GridLayout</code> object.
     * <p/>
     * The grid layout manager determines the size of individual
     * components by dividing the free space in the container into
     * equal-sized portions according to the number of rows and columns
     * in the layout. The container's free space equals the container's
     * size minus any insets and any specified horizontal or vertical
     * gap. All components in a grid layout are given the same size.
     *
     * @param parent the container in which to do the layout
     * @see java.awt.Container
     * @see java.awt.Container#doLayout
     */
    public void layoutContainer(Container parent) {
        synchronized (parent.getTreeLock()) {
            layout.layoutContainer(parent);
        }
    }

    /**
     * Returns the string representation of this grid layout's values.
     *
     * @return a string representation of this grid layout
     */
    public String toString() {
        return getClass().getName() + "[gap=" + cgap +
                ",rows=" + cpl + ",cols=" + lines + "]";
    }

    /**
     * An enum defining the layout styles available
     *
     * @author A.G.Docherty
     * @date 19 Jun 2009
     */
    public enum HexLayoutStyle {
        WAVEY {
            @Override
            int numLines(int nComponents, int maxPerLine) {
                return (nComponents + maxPerLine - 1) / maxPerLine;
            }

            @Override
            int numComponentsThisLine(int ncols, int line) {
                return ncols;
            }

            @Override
            int maxComponentsPerLine(int nComponents, int nLines) {
                return (nComponents + nLines - 1) / nLines;
            }
        },

        STRAIGHT_MIN_FIRST {
            @Override
            int numLines(int nComponents, int maxPerLine) {
                int lines = 0;
                int remaining = nComponents;
                boolean minLine = true;

                while (remaining > 0) {
                    lines++;
                    remaining -= maxPerLine - (minLine ? 1 : 0);

                    minLine = !minLine;
                }

                return lines;
            }

            @Override
            int numComponentsThisLine(int ncols, int line) {
                int adj = line % 2;
                return ncols - adj;
            }

            @Override
            int maxComponentsPerLine(int nComponents, int nLines) {
                int missing = (nLines + 1) / 2;
                nComponents += missing;

                int cpl = (nComponents + nLines - 1) / nLines;
                return cpl;
            }
        },

        STRAIGHT_MAX_FIRST {
            @Override
            int numLines(int nComponents, int maxPerLine) {
                int lines = 0;
                int remaining = nComponents;
                boolean minLine = false;

                while (remaining > 0) {
                    lines++;
                    remaining -= maxPerLine - (minLine ? 1 : 0);

                    minLine = !minLine;
                }

                return lines;
            }

            @Override
            int numComponentsThisLine(int ncols, int line) {
                int adj = ++line % 2;
                return ncols - adj;
            }

            @Override
            int maxComponentsPerLine(int nComponents, int nLines) {
                int missing = nLines / 2;
                nComponents += missing;

                int cpl = (nComponents + nLines - 1) / nLines;
                return cpl;
            }
        };

        abstract int numComponentsThisLine(int ncols, int line);

        abstract int numLines(int nComponents, int maxPerLine);

        abstract int maxComponentsPerLine(int nComponents, int nLines);

    }

    /**
     * An abstract layout inner class to layout components with different hexagonal orientations
     *
     * @author A.G.Docherty
     * @date 13 Jun 2009
     */
    private abstract class AbstractLayout {
        Dimension preferredLayoutSize(Container parent) {
            Insets insets = parent.getInsets();
            int ncomponents = parent.getComponentCount();
            int nrows = cpl;
            int ncols = lines;

            if (nrows > 0) {
                ncols = (ncomponents + nrows - 1) / nrows;
            } else {
                nrows = (ncomponents + ncols - 1) / ncols;
            }

            int w = 0;
            int h = 0;
            for (int i = 0; i < ncomponents; i++) {
                Component comp = parent.getComponent(i);
                Dimension d = comp.getPreferredSize();
                if (w < d.width) {
                    w = d.width;
                }
                if (h < d.height) {
                    h = d.height;
                }
            }

            int dx = insets.left + insets.right + ncols * w + (ncols - 1) * cgap;
            int dy = insets.top + insets.bottom + nrows * h + (nrows - 1) * cgap;

            if (nrows > 1) {
                dx = adjustOverlapX(dx, w, nrows, ncols);
                dy = adjustOverlapY(dy, h, nrows, ncols);
            }

            return new Dimension(dx, dy);
        }

        Dimension minimumLayoutSize(Container parent) {
            Insets insets = parent.getInsets();
            int ncomponents = parent.getComponentCount();
            int nrows = cpl;
            int ncols = lines;

            if (nrows > 0) {
                ncols = (ncomponents + nrows - 1) / nrows;
            } else {
                nrows = (ncomponents + ncols - 1) / ncols;
            }
            int w = 0;
            int h = 0;
            for (int i = 0; i < ncomponents; i++) {
                Component comp = parent.getComponent(i);
                Dimension d = comp.getMinimumSize();
                if (w < d.width) {
                    w = d.width;
                }
                if (h < d.height) {
                    h = d.height;
                }
            }

            int dx = insets.left + insets.right + ncols * w + (ncols - 1) * cgap;
            int dy = insets.top + insets.bottom + nrows * h + (nrows - 1) * cgap;

            if (nrows > 1) {
                dx = adjustOverlapX(dx, w, nrows, ncols);
                dy = adjustOverlapY(dy, h, nrows, ncols);
            }

            return new Dimension(dx, dy);
        }

        abstract int adjustOverlapX(int x, int w, int nrows, int ncols);

        abstract int adjustOverlapY(int y, int h, int nrows, int ncols);

        abstract void layoutContainer(Container parent);
    }

    /**
     * An layout inner class to layout components with a vertical hexagonal orientation
     *
     * @author A.G.Docherty
     * @date 13 Jun 2009
     */
    private class VerticalLayout extends AbstractLayout {

        int adjustOverlapX(int x, int w, int nrows, int ncols) {
            if (nrows > 1) {
                x = x + (int) (w * 0.5f);
            }

            return x;
        }

        int adjustOverlapY(int y, int h, int nrows, int ncols) {
            if (nrows > 1) {
                y /= nrows;
                y = y + (int) (y * (nrows - 1) * 0.75f);
            }

            return y;
        }

        void layoutContainer(Container parent) {
            Insets insets = parent.getInsets();
            int ncomponents = parent.getComponentCount();
            int ncols = cpl;
            int nrows = lines;

            if (ncomponents == 0) {
                return;
            }

            if (nrows == 0 && ncols == 0) {
                ncols = (int) (Math.sqrt(ncomponents) + 0.7);
            }

            if (ncols == 0) {
                ncols = layoutStyle.maxComponentsPerLine(ncomponents, nrows);
            } else {
                int n = layoutStyle.numLines(ncomponents, ncols);

                if (n > nrows)
                    nrows = n;
            }

            int width = parent.getWidth() - (insets.left + insets.right) - 1;
            int height = parent.getHeight() - (insets.top + insets.bottom) - 1;
            int w = 0;

            if (layoutStyle == HexLayoutStyle.WAVEY)
                w = (int) (((width - (ncols - 1) * cgap) / (ncols + (nrows > 1 ? 0.5f : 0.0f))) + 0.5);
            else
                w = (int) (((width - (ncols - 1) * cgap) / ncols) + 0.5);

            float effectiveRows = 1 + ((nrows - 1) * 0.75f);
            int h = (int) ((height - (nrows - 1) * cgap) / effectiveRows);

            if (!stretchy) {
                // adjust the height/width to ensure pure hexagons are displayed
                if (h / w > ASPECT_RATIO) {
                    int newH = (int) ((w / ASPECT_RATIO) + 0.5);

                    if ((newH + ((nrows - 1) * cgap)) * effectiveRows > height) {
                        int newW = (int) ((h * ASPECT_RATIO) + 0.5);

                        if (newW > w) {
                            h = (int) ((w / ASPECT_RATIO) + 0.5);
                        } else {
                            w = newW;
                        }
                    } else {
                        h = newH;
                    }
                } else {
                    w = (int) (h * ASPECT_RATIO);
                }
            }

            int xoffset = (w + cgap) / 2;
            int yoffset = (int) ((h * 0.75f) + 0.5f);
            boolean staggeredRow = layoutStyle == HexLayoutStyle.STRAIGHT_MIN_FIRST;
            int index = 0;

            // set the bounds of each component
            for (int r = 0, y = insets.top; r < nrows; r++, y += yoffset + cgap) {
                int offset = 0;

                if (staggeredRow)
                    offset = xoffset;

                int maxC = layoutStyle.numComponentsThisLine(ncols, r + 1);

                for (int c = 0, x = insets.left; c < maxC; c++, x += w + cgap) {
//            int i = c * nrows + r;
                    if (index < ncomponents) {
                        Component comp = parent.getComponent(index);

                        if (comp instanceof IHexagonal) {
                            ((IHexagonal) comp).setOrientation(orientation);
                        }

                        comp.setBounds(x + offset, y, w, h);
                    }

                    index++;
                }

                staggeredRow = !staggeredRow;
            }
        }
    }

    /**
     * An layout inner class to layout components with a horizontal hexagonal orientation
     *
     * @author A.G.Docherty
     * @date 13 Jun 2009
     */
    private class HorizontalLayout extends AbstractLayout {

        int adjustOverlapX(int x, int w, int nrows, int ncols) {
            if (ncols > 1) {
                x /= ncols;
                x = x + (int) (x * (ncols - 1) * 0.75f);
            }

            return x;
        }

        int adjustOverlapY(int y, int h, int nrows, int ncols) {
            if (ncols > 1) {
                y = y + (int) (h * 0.5f);
            }

            return y;
        }

        void layoutContainer(Container parent) {
            Insets insets = parent.getInsets();
            int ncomponents = parent.getComponentCount();
            int ncols = lines;
            int nrows = cpl;

            if (ncomponents == 0) {
                return;
            }

            if (nrows == 0 && ncols == 0) {
                nrows = (int) (Math.sqrt(ncomponents) + 0.7);
            }

            if (nrows == 0) {
                nrows = layoutStyle.maxComponentsPerLine(ncomponents, ncols);
            } else {
                int n = layoutStyle.numLines(ncomponents, nrows);

                if (n > ncols)
                    ncols = n;
            }

            int width = parent.getWidth() - (insets.left + insets.right) - 1;
            int height = parent.getHeight() - (insets.top + insets.bottom) - 1;
            int h = 0;

            if (layoutStyle == HexLayoutStyle.WAVEY)
                h = (int) (((height - (nrows - 1) * cgap) / (nrows + (ncols > 1 ? 0.5f : 0.0f))) + 0.5);
            else
                h = (int) (((height - (nrows - 1) * cgap) / nrows) + 0.5);


            float effectiveCols = 1 + ((ncols - 1) * 0.75f);
            int w = (int) ((width - (ncols - 1) * cgap) / effectiveCols);

            if (!stretchy) {
                // adjust the height/width to ensure pure hexagons are displayed
                if (w / h > ASPECT_RATIO) {
                    int newW = (int) ((h / ASPECT_RATIO) + 0.5);

                    if ((newW + ((ncols - 1) * cgap)) * effectiveCols > width) {
                        int newH = (int) ((w * ASPECT_RATIO) + 0.5);

                        if (newH > h) {
                            w = (int) ((h / ASPECT_RATIO) + 0.5);
                        } else {
                            h = newH;
                        }
                    } else {
                        w = newW;
                    }
                } else {
                    h = (int) (w * ASPECT_RATIO);
                }
            }

            int yoffset = (h + cgap) / 2;
            int xoffset = (int) ((w * 0.75f) + 0.5f);
            boolean staggeredCol = layoutStyle == HexLayoutStyle.STRAIGHT_MIN_FIRST;
            int index = 0;

            // set the bounds of each component
            for (int c = 0, x = insets.left; c < ncols; c++, x += xoffset + cgap) {
                int offset = 0;

                if (staggeredCol)
                    offset = yoffset;

                int maxC = layoutStyle.numComponentsThisLine(nrows, c + 1);

                for (int r = 0, y = insets.top; r < maxC; r++, y += h + cgap) {
                    if (index < ncomponents) {
                        Component comp = parent.getComponent(index);

                        if (comp instanceof IHexagonal) {
                            ((IHexagonal) comp).setOrientation(orientation);
                        }

                        comp.setBounds(x, y + offset, w, h);
                    }

                    index++;
                }

                staggeredCol = !staggeredCol;
            }
        }
    }
}
