
/**
 *
 * @author Mario
 */
import java.awt.*;
import javax.swing.*;

public class GridTest extends JFrame {

    private static final long serialVersionUID = 6632092242560855625L;
    static JPanel gridPane;
    static JViewport view;

    public static void main(String[] args) {
        GridTest frame = new GridTest();
        frame.setVisible(true);
    }

    public GridTest() {
        setTitle("Grid Test");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(600, 600);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());

        JScrollPane scrollPane = new JScrollPane();
        setContentPane(scrollPane);
        view = scrollPane.getViewport();
        gridPane = new JPanel() {
            private static final long serialVersionUID = 2900962087641689502L;

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                drawGrid(g, view.getViewRect());
            }
        };
        Dimension paneSize = new Dimension(28672, 14336);
        gridPane.setPreferredSize(paneSize);
        gridPane.setBackground(Color.gray);
        scrollPane.setViewportView(gridPane);
    }

    static void drawGrid(Graphics g, Rectangle view) {
        int wMax = gridPane.getWidth();
        int hMax = gridPane.getHeight();

        g.setColor(Color.black);
        Rectangle tile = view;
        // set corner tile x/y to the tile increment.
        for (int w = 0; w < wMax; w += 1024) {
            if (tile.x >= w && tile.x < w + 1024) {
                tile.x = (w);
            }
            for (int h = 0; h < hMax; h += 1024) {
                if (tile.y >= h && tile.y < h + 1024) {
                    tile.y = (h);
                }
            }
        }
        int xTop = tile.x;
        int yTop = tile.y;
        int width = (int) tile.getWidth();
        int height = (int) tile.getHeight();
        width = xTop + width;
        height = yTop + height;
        // Draw even grid squares within visible tiles, starting at top corner tile.
        for (int w = xTop; w < width + 1024; w += 64) {
            for (int h = yTop; h < height + 1024; h += 128) {
                g.fillRect(w + 1, h + 1, 63, 63);
            }
        }
        // Draw odd grid squares within visible tiles, starting at top corner tile.
        for (int w = xTop - 32; w < width + 1024; w += 64) {
            for (int h = yTop + 64; h < height + 1024; h += 128) {
                g.fillRect(w + 1, h + 1, 63, 63);
            }
        }
    }
}