package hierarchical_enums;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Implementation example of hierarchical enums
 *
 * @author
 * http://blog.cedarsoft.com/2010/10/hierarchical-structures-with-java-enums/
 */
public enum OsType {

    OS(null),
    Windows(OS),
    WindowsNT(Windows),
    WindowsNTWorkstation(WindowsNT),
    WindowsNTServer(WindowsNT),
    Windows2000(Windows),
    Windows2000Server(Windows2000),
    Windows2000Workstation(Windows2000),
    WindowsXp(Windows),
    WindowsVista(Windows),
    Windows7(Windows),
    Windows95(Windows),
    Windows98(Windows),
    Unix(OS) {

        @Override
        public boolean supportsXWindowSystem() {
            return true;
        }
    },
    Linux(Unix),
    AIX(Unix),
    HpUx(Unix),
    SunOs(Unix),;
    private final OsType parent;
    private final List children = new ArrayList();
    private final List allChildren = new ArrayList();

    OsType(OsType parent) {
        this.parent = parent;
        if (parent != null) {
            parent.addChild(this);
        }
    }

    public OsType parent() {
        return parent;
    }

    public boolean is(OsType other) {
        if (other == null) {
            return false;
        }

        for (OsType osType = this; osType != null; osType = osType.parent()) {
            if (other == osType) {
                return true;
            }
        }
        return false;
    }

    public List children() {
        return Collections.unmodifiableList(children);
    }

    public List allChildren() {
        return Collections.unmodifiableList(allChildren);
    }

    private void addChild(OsType child) {
        this.children.add(child);

        List greatChildren = new ArrayList();
        greatChildren.add(child);
        greatChildren.addAll(child.allChildren());

        OsType currentAncestor = this;
        while (currentAncestor != null) {
            currentAncestor.allChildren.addAll(greatChildren);
            currentAncestor = currentAncestor.parent;
        }
    }

    public boolean supportsXWindowSystem() {
        if (parent == null) {
            return false;
        }

        return parent.supportsXWindowSystem();
    }
}
